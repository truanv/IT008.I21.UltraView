﻿
using UltraViewer.Properties;
namespace UltraViewer
{
    partial class formMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();

                language.Dispose();
                mySlave.Dispose();
                screen.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(formMain));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.systemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.languageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vietnameseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.englishToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listView1 = new System.Windows.Forms.ListView();
            this.label1Sever = new System.Windows.Forms.Label();
            this.listView2 = new System.Windows.Forms.ListView();
            this.label1Client = new System.Windows.Forms.Label();
            this.label2Sever = new System.Windows.Forms.Label();
            this.label2Client = new System.Windows.Forms.Label();
            this.txbLocalIP = new System.Windows.Forms.TextBox();
            this.label3Sever = new System.Windows.Forms.Label();
            this.label3Client = new System.Windows.Forms.Label();
            this.txbRemoteIP = new System.Windows.Forms.TextBox();
            this.btnStart = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tsslblConnectState = new System.Windows.Forms.ToolStripStatusLabel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1Sever = new System.Windows.Forms.PictureBox();
            this.tmrComing = new System.Windows.Forms.Timer(this.components);
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1Sever)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.SkyBlue;
            resources.ApplyResources(this.menuStrip1, "menuStrip1");
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.systemToolStripMenuItem});
            this.menuStrip1.Name = "menuStrip1";
            // 
            // systemToolStripMenuItem
            // 
            this.systemToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.languageToolStripMenuItem,
            this.helpToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.systemToolStripMenuItem.Image = global::UltraViewer.Properties.Resources.setting;
            this.systemToolStripMenuItem.Name = "systemToolStripMenuItem";
            resources.ApplyResources(this.systemToolStripMenuItem, "systemToolStripMenuItem");
            // 
            // languageToolStripMenuItem
            // 
            this.languageToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.vietnameseToolStripMenuItem,
            this.englishToolStripMenuItem});
            this.languageToolStripMenuItem.Image = global::UltraViewer.Properties.Resources.Languages;
            this.languageToolStripMenuItem.Name = "languageToolStripMenuItem";
            resources.ApplyResources(this.languageToolStripMenuItem, "languageToolStripMenuItem");
            // 
            // vietnameseToolStripMenuItem
            // 
            this.vietnameseToolStripMenuItem.Checked = true;
            this.vietnameseToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.vietnameseToolStripMenuItem.Image = global::UltraViewer.Properties.Resources.vietnam;
            this.vietnameseToolStripMenuItem.Name = "vietnameseToolStripMenuItem";
            resources.ApplyResources(this.vietnameseToolStripMenuItem, "vietnameseToolStripMenuItem");
            this.vietnameseToolStripMenuItem.Click += new System.EventHandler(this.vietnameseToolStripMenuItem_Click);
            // 
            // englishToolStripMenuItem
            // 
            this.englishToolStripMenuItem.Image = global::UltraViewer.Properties.Resources.english;
            this.englishToolStripMenuItem.Name = "englishToolStripMenuItem";
            resources.ApplyResources(this.englishToolStripMenuItem, "englishToolStripMenuItem");
            this.englishToolStripMenuItem.Click += new System.EventHandler(this.englishToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Image = global::UltraViewer.Properties.Resources.help;
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            resources.ApplyResources(this.helpToolStripMenuItem, "helpToolStripMenuItem");
            this.helpToolStripMenuItem.Click += new System.EventHandler(this.helpToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Image = global::UltraViewer.Properties.Resources.exit;
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            resources.ApplyResources(this.exitToolStripMenuItem, "exitToolStripMenuItem");
            // 
            // listView1
            // 
            resources.ApplyResources(this.listView1, "listView1");
            this.listView1.Name = "listView1";
            this.listView1.UseCompatibleStateImageBehavior = false;
            // 
            // label1Sever
            // 
            resources.ApplyResources(this.label1Sever, "label1Sever");
            this.label1Sever.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label1Sever.Name = "label1Sever";
            // 
            // listView2
            // 
            resources.ApplyResources(this.listView2, "listView2");
            this.listView2.Name = "listView2";
            this.listView2.UseCompatibleStateImageBehavior = false;
            // 
            // label1Client
            // 
            resources.ApplyResources(this.label1Client, "label1Client");
            this.label1Client.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label1Client.Name = "label1Client";
            // 
            // label2Sever
            // 
            resources.ApplyResources(this.label2Sever, "label2Sever");
            this.label2Sever.Name = "label2Sever";
            // 
            // label2Client
            // 
            resources.ApplyResources(this.label2Client, "label2Client");
            this.label2Client.Name = "label2Client";
            // 
            // txbLocalIP
            // 
            resources.ApplyResources(this.txbLocalIP, "txbLocalIP");
            this.txbLocalIP.Name = "txbLocalIP";
            this.txbLocalIP.ReadOnly = true;
            // 
            // label3Sever
            // 
            resources.ApplyResources(this.label3Sever, "label3Sever");
            this.label3Sever.Name = "label3Sever";
            // 
            // label3Client
            // 
            resources.ApplyResources(this.label3Client, "label3Client");
            this.label3Client.Name = "label3Client";
            // 
            // txbRemoteIP
            // 
            resources.ApplyResources(this.txbRemoteIP, "txbRemoteIP");
            this.txbRemoteIP.Name = "txbRemoteIP";
            // 
            // btnStart
            // 
            resources.ApplyResources(this.btnStart, "btnStart");
            this.btnStart.Name = "btnStart";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            resources.ApplyResources(this.statusStrip1, "statusStrip1");
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsslblConnectState});
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.SizingGrip = false;
            // 
            // tsslblConnectState
            // 
            this.tsslblConnectState.BackColor = System.Drawing.SystemColors.Control;
            resources.ApplyResources(this.tsslblConnectState, "tsslblConnectState");
            this.tsslblConnectState.Image = global::UltraViewer.Properties.Resources.NotConnected;
            this.tsslblConnectState.Name = "tsslblConnectState";
            // 
            // pictureBox2
            // 
            resources.ApplyResources(this.pictureBox2, "pictureBox2");
            this.pictureBox2.Image = global::UltraViewer.Properties.Resources.image2;
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1Sever
            // 
            resources.ApplyResources(this.pictureBox1Sever, "pictureBox1Sever");
            this.pictureBox1Sever.Image = global::UltraViewer.Properties.Resources.image1;
            this.pictureBox1Sever.Name = "pictureBox1Sever";
            this.pictureBox1Sever.TabStop = false;
            // 
            // tmrComing
            // 
            this.tmrComing.Tick += new System.EventHandler(this.tmrComing_Tick);
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            resources.ApplyResources(this.comboBox1, "comboBox1");
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.SelectedValueChanged += new System.EventHandler(this.comboBox1_SelectedValueChanged);
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // formMain
            // 
            this.AcceptButton = this.btnStart;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.txbRemoteIP);
            this.Controls.Add(this.label3Client);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label3Sever);
            this.Controls.Add(this.txbLocalIP);
            this.Controls.Add(this.label2Client);
            this.Controls.Add(this.label2Sever);
            this.Controls.Add(this.label1Client);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.listView2);
            this.Controls.Add(this.label1Sever);
            this.Controls.Add(this.pictureBox1Sever);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "formMain";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.formMain_FormClosing);
            this.Load += new System.EventHandler(this.formMain_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1Sever)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStripMenuItem systemToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem languageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vietnameseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem englishToolStripMenuItem;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.PictureBox pictureBox1Sever;
        private System.Windows.Forms.Label label1Sever;
        private System.Windows.Forms.ListView listView2;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label1Client;
        private System.Windows.Forms.Label label2Sever;
        private System.Windows.Forms.Label label2Client;
        private System.Windows.Forms.TextBox txbLocalIP;
        private System.Windows.Forms.Label label3Sever;
        private System.Windows.Forms.Label label3Client;
        private System.Windows.Forms.TextBox txbRemoteIP;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel tsslblConnectState;
        private System.Windows.Forms.Timer tmrComing;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label1;
    }
}

