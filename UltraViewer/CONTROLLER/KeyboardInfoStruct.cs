﻿namespace CONTROLLER
{
    public enum KeypressInfo
    {
        Down,
        Up
    }

   public struct KeyInfo
    {
        public KeypressInfo action;

        public byte keycode;
        
        public KeyInfo(KeypressInfo _action, byte _keycode)
        {
            keycode = _keycode;
            action = _action;
        }

    }
}
