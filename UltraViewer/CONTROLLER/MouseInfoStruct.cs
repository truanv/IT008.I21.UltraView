﻿namespace CONTROLLER
{
    public struct MouseInfo
    {
        public SimWinInput.SimMouse.Action action;
        public int X;
        public int Y;

        public MouseInfo(SimWinInput.SimMouse.Action _action, int _x, int _y)
        {
            action = _action;
            X = _x;
            Y = _y;
        }
        public MouseInfo(SimWinInput.SimMouse.Action _action, System.Drawing.Point inputPositions)
        {
            action = _action;
            X = inputPositions.X;
            Y = inputPositions.Y;
        }
    }
}
