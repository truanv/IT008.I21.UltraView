﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Drawing;
using System.Windows.Input;
using System.Windows.Forms;
using System.Collections;
using System.ComponentModel;
using System.Reflection;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;

namespace CONTROLLER
{
    class DataConverter
    {
        
        public DataType Type { get; private set; }
        public byte[] Data { get; private set; }

        public DataConverter(byte[] _byteArray)
        {
            int dataSize = _byteArray.Length;

            if(dataSize > 4)
            {
                Data = new byte[dataSize - 4];

                Type = (DataType)BitConverter.ToInt32(_byteArray, dataSize - 4);

                Array.Copy(_byteArray, 0, Data, 0, dataSize - 4);
            }
        }

        public static byte[] FromBitmap(Bitmap _bitmap)
        {
            using (var ms = new MemoryStream())
            {
                int type = (int)DataType.Bitmap;

                _bitmap.Save(ms, ImageFormat.Jpeg);
                byte[] data = ms.ToArray();
                return data.Concat(BitConverter.GetBytes(type)).ToArray();
            }
        }

        public Bitmap ToBitmap()
        {
            var ms = new MemoryStream(Data);
            Bitmap _data = new Bitmap(ms);
            return _data;
        }

        public static byte[] FromKey(KeyInfo _data)
        {
            int size = Marshal.SizeOf(_data);
            byte[] arr = new byte[size];
            IntPtr ptr = Marshal.AllocHGlobal(size);
            Marshal.StructureToPtr(_data, ptr, true);
            Marshal.Copy(ptr, arr, 0, size);
            Marshal.FreeHGlobal(ptr);

            int type = (int)DataType.Key;
            return arr.Concat(BitConverter.GetBytes(type)).ToArray();
        }

        public KeyInfo ToKey()
        {
            KeyInfo _keyinfo = new KeyInfo();
            GCHandle handle = GCHandle.Alloc(Data, GCHandleType.Pinned);
            try
            {
                _keyinfo = (KeyInfo)Marshal.PtrToStructure(handle.AddrOfPinnedObject(), typeof(KeyInfo));
            }
            finally
            {
                handle.Free();
            }

            return _keyinfo;
        }

        public static byte[] FromMouse(MouseInfo _data)
        {
            int size = Marshal.SizeOf(_data);
            byte[] arr = new byte[size];
            IntPtr ptr = Marshal.AllocHGlobal(size);
            Marshal.StructureToPtr(_data, ptr, true);
            Marshal.Copy(ptr, arr, 0, size);
            Marshal.FreeHGlobal(ptr);

            int type = (int)DataType.Mouse;

            return arr.Concat(BitConverter.GetBytes(type)).ToArray();
            //return Encoding.Unicode.GetBytes(_mouseButton.ToString()).Concat(BitConverter.GetBytes(type)).ToArray();
        }

        public MouseInfo ToMouse()
        {
            MouseInfo _mouseInfo = new MouseInfo();
            GCHandle handle = GCHandle.Alloc(Data, GCHandleType.Pinned);
            try
            {
                _mouseInfo = (MouseInfo)Marshal.PtrToStructure(handle.AddrOfPinnedObject(), typeof(MouseInfo));
            }
            finally
            {
                handle.Free();
            }
            return _mouseInfo;
        }

        public static byte[] FromPositionMouse(Point _position)
        {
            using (var ms = new MemoryStream())
            {
                var bw = new BinaryWriter(ms);
                bw.Write(_position.X);
                bw.Write(_position.Y);
                int type = (int)DataType.Pos;
                byte[] _byte = ms.ToArray();
                return _byte.Concat(BitConverter.GetBytes(type)).ToArray();
            }
        }

        public Point ToPositionMouse()
        {
            using (var ms = new MemoryStream(Data))
            {
                var r = new BinaryReader(ms);
                return new Point(r.ReadInt32(), r.ReadInt32());
            }
        }

        public static byte[] FromMouseWheel(int valueWheel)
        {
            int type = (int)DataType.Wheel;
            byte[] _data = BitConverter.GetBytes(valueWheel);
            _data = _data.Concat(BitConverter.GetBytes(type)).ToArray();
            return _data;
        }

        public int ToValueWheel()
        {
            return BitConverter.ToInt32(Data, 0);
        }

        public object AutoConverter()
        {
            if (Type == DataType.Bitmap)
            {
                return ToBitmap();
            }
            else if (Type == DataType.Key)
            {
                return ToKey();
            }
            else if (Type == DataType.Mouse)
            {
                return ToMouse();
            }
            else if (Type == DataType.Pos)
            {
                return ToPositionMouse();
            }
            else if(Type == DataType.Wheel)
            {
                return ToValueWheel();
            }
            else if(Type == DataType.BlockInput)
            {
                return ToStateBlockInput();
            }
            else
            {
                return null;
            }
           
        }


        public static byte[] FromStateBlockInput(bool _state)
        {
            int type = (int)DataType.BlockInput;
            byte[] _data = BitConverter.GetBytes(_state);
            _data = _data.Concat(BitConverter.GetBytes(type)).ToArray();
            return _data;
        }
        
        public bool ToStateBlockInput()
        {
            bool resutl = BitConverter.ToBoolean(Data, 0);
            return resutl;
        }

    }
}
