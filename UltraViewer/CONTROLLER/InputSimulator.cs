﻿using System;
using System.Drawing;
using System.Windows.Forms;


namespace CONTROLLER
{
    class InputSimulator
    {
        static Rectangle myScreen;

        static InputSimulator()
        {
            myScreen = Screen.PrimaryScreen.Bounds;
        }

        public static void Simulate(Object _input, DataType _type)
        {
            switch (_type)
            {
                case DataType.Pos:
                    Point _pos = (Point)_input;
                    SimWinInput.SimMouse.Act(SimWinInput.SimMouse.Action.MoveOnly, _pos.X, _pos.Y);
                    break;

                case DataType.Mouse:
                    MouseInfo mouseInfo = (MouseInfo)_input;
                    SimWinInput.SimMouse.Act(mouseInfo.action, Cursor.Position.X, Cursor.Position.Y);
                    break;

                case DataType.Key:
                    KeyInfo keyInfo = (KeyInfo)_input;
                    if (keyInfo.action == KeypressInfo.Down)
                    {
                        SimWinInput.SimKeyboard.KeyDown(keyInfo.keycode);
                    }
                    else
                    {
                        SimWinInput.InteropKeyboard.keybd_event(keyInfo.keycode, 0,
                            (uint)SimWinInput.InteropKeyboard.KeyboardEventFlags.KeyUp
                            | (uint)SimWinInput.InteropKeyboard.KeyboardEventFlags.KeyDown, 0);
                    }
                    break;

                case DataType.Wheel:
                    int delta = (int)_input;
                    SimWinInput.InteropMouse.mouse_event((uint)SimWinInput.InteropMouse.MouseEventFlags.Wheel, 0, 0, delta, 0);
                    break;

                case DataType.BlockInput:
                    bool _state = (bool)_input;
                    BlockInput.Toggle(_state);
                    break;
            }
        }
    }
}
