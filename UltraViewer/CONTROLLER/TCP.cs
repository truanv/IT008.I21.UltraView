﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CONTROLLER
{
    public class CoreFunction
    {
        public bool Connected { get; protected set; }

        public byte[] Data { get; private set; }

        protected int Port;

        protected Socket conSock;

        protected IPEndPoint endPoint;

        protected CoreFunction()
        {
            Connected = false;
            Port = 14634;
        }

        public int SendVarData(byte[] sendData)
        {
            if (!conSock.Connected || !Connected)
            {
                Connected = conSock.Connected;
                Debug.WriteLine("Send socket hasn't connected or NOT READY!", "SendVarData");
                return 0;
            }

            int sended = 0;
            byte[] dataSizeBuff = new byte[4];
            dataSizeBuff = BitConverter.GetBytes(sendData.Length);

            try
            {
                sended = conSock.Send(dataSizeBuff);

            }
            catch (SocketException e)
            {
                //An error occurred when attempting to access the socket
                Connected = false;
                Debug.WriteLine(e.Message, "SendVarData");
                return -1;

            }
            catch (ObjectDisposedException ee)
            {
                //The Socket has been closed
                Connected = false;
                Debug.WriteLine(ee.Message, "SendVarData");
                return -2;
            }

            int totalSend = 0;
            int sendLeft = sendData.Length;

            int packetSize = conSock.SendBufferSize;

            try
            {
                while (totalSend < sendData.Length)
                {
                    int packedCount = sendLeft / packetSize;
                    int lastPackedSize = sendLeft % packetSize;

                    if (packedCount <= 1)
                    {
                        sended = conSock.Send(sendData);
                        totalSend += sended;
                    }
                    else
                    {
                        for (int i = 0; i < packedCount; i++)
                        {
                            sended = conSock.Send(sendData, totalSend, packetSize, SocketFlags.None);
                            totalSend += sended;
                        }
                        //send last packet
                        sended = conSock.Send(sendData, totalSend, lastPackedSize, SocketFlags.None);
                        totalSend += sended;
                    }
                    sendLeft -= totalSend;
                }
            }
            catch (SocketException e)
            {
                //socketFlags is not a valid combination of values || An error occurred when attempting to access the socket
                Connected = false;
                Debug.WriteLine(e.Message, "SendVarData");
                return -1;
            }
            catch (ObjectDisposedException ee)
            {
                //The Socket has been closed
                Connected = false;
                Debug.WriteLine(ee.Message, "SendVarData");
                return -2;
            }

            return sendData.Length;
        }

        public int ReceiveVarData()
        {
            if (!Connected)
            {
                //not connected
                Debug.WriteLine("not connected", "ReceiveVarData");
                return -3;
            }

            byte[] dataSizeBuff = new byte[4];

            try
            {
                if (conSock != null && conSock.Available == 0)
                {
                    //there's nothing to read
                    return 0;
                }

                //receive the actual size of data
                conSock.Receive(dataSizeBuff, 4, SocketFlags.None);
            }
            catch (SocketException e)
            {
                //An error occurred when attempting to access the socket
                Connected = false;
                Debug.WriteLine(e.Message, "ReceiveVarData");
                return -1;

            }
            catch (ObjectDisposedException ee)
            {
                //The Socket has been closed
                Connected = false;
                Debug.WriteLine(ee.Message, "ReceiveVarData");
                return -2;
            }

            int dataSize = BitConverter.ToInt32(dataSizeBuff, 0);
            Data = new byte[dataSize];

            int received = 0;
            int totalRecv = 0;
            int leftRecv = dataSize;

            while (totalRecv < dataSize)
            {
                try
                {
                    received = conSock.Receive(Data, totalRecv, leftRecv, SocketFlags.None);

                    if (received != 0)
                    {
                        totalRecv += received;
                        leftRecv -= received;
                    }
                }
                catch (SocketException e)
                {
                    //An error occurred when attempting to access the socket
                    Connected = false;
                    Debug.WriteLine(e.Message, "ReceiveVarData_ReceingData");
                    return -1;

                }
                catch (ObjectDisposedException ee)
                {
                    //The Socket has been closed
                    Connected = false;
                    Debug.WriteLine(ee.Message, "ReceiveVarData_ReceingData");
                    return -2;
                }
            }
            return dataSize;
        }

        public string GetRemoteIPAddress()
        {
            if (!Connected)
            {
                //Hasn't connected to remote
                Debug.WriteLine("not connected", "GetRemoteIPAddress");
                return null;
            }
            return IPAddress.Parse(((IPEndPoint)conSock.RemoteEndPoint).Address.ToString()).ToString();
        }

    }

    public class Slave : CoreFunction, IDisposable
    {
        Socket waitSock;

        public Slave()
        {
            waitSock = new Socket(SocketType.Stream, ProtocolType.Tcp);
            waitSock.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);

            endPoint = new IPEndPoint(IPAddress.Any, Port);
        }

        public int StartListen()
        {
            if (!waitSock.IsBound)
            {
                try
                {
                    waitSock.Bind(endPoint);
                }
                catch (SocketException e)
                {
                    //An error occurred when attempting to access the socket
                    Debug.WriteLine(e.Message, "StartListen");
                    return -1;

                }
                catch (ObjectDisposedException ee)
                {
                    //The Socket has been closed
                    Debug.WriteLine(ee.Message, "StartListen");
                    return -2;
                }
            }

            
            try
            {
                waitSock.Listen(1);
                Thread listenThread = new Thread(WaitingConnection);
                listenThread.IsBackground = true;
                listenThread.Start();
            }
            catch (SocketException e)
            {
                //An error occurred when attempting to access the socket
                Debug.WriteLine(e.Message, "StartListen_CreateThread");
                return -1;

            }
            catch (ObjectDisposedException ee)
            {
                //The Socket has been closed
                Debug.WriteLine(ee.Message, "StartListen_CreateThread");
                return -2;
            }
            catch (Exception eee)
            {
                //some ugly things happen :<<
                Debug.WriteLine(eee.Message, "StartListen_CreateThread");
                return -3;
            }

            return 1;
        }

        private void WaitingConnection()
        {
            if (conSock != null && conSock.Connected)
            {
                //already connected;
                Debug.WriteLine("Already connected to remote host", "WaitingConnection");
                return;
            }

            try
            {
                conSock = waitSock.Accept();
                Connected = true;
            }
            catch (SocketException e)
            {
                //An error occurred when attempting to access the socket
                Debug.WriteLine(e.Message, "WaitingConnection");
                return;

            }
            catch (ObjectDisposedException ee)
            {
                //The Socket has been closed
                Debug.WriteLine(ee.Message, "WaitingConnection");
                return;
            }
            catch (InvalidOperationException eee)
            {
                //The accepting socket is not listening for connections. You must call Bind(EndPoint) and Listen(Int32) before calling Accept().
                Debug.WriteLine(eee.Message, "WaitingConnection");
                return;
            }
        }

        public void RefuseConnection()
        {
            Connected = false;
            conSock.Close();
        }

        public void Renew()
        {
            conSock.Close();
            conSock = null;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // dispose managed resources
                try
                {
                    conSock.Close();
                    waitSock.Close();
                }
                catch (Exception) { }
                
            }
            // free native resources
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }

    public class Master : CoreFunction
    {
        IPAddress remoteIP;

        public Master()
        {
            
        }

        public void CreateSocket()
        {
            conSock = new Socket(SocketType.Stream, ProtocolType.Tcp);
        }

        public void StartConnect(IPAddress ip)
        {
            remoteIP = ip;
                
            Thread connectThread = new Thread(TryToConnect);
            connectThread.IsBackground = true;
            connectThread.Start();

            //Try to connect and block caller for 5sec then terminate if it hasn't completed
            connectThread.Join(5000);
        }

        private void TryToConnect()
        {
            try
            {
                conSock.Connect(remoteIP, Port);
                Connected = true;
            }
            catch (ArgumentOutOfRangeException e)
            {
                //The port number is not valid.
                Debug.WriteLine(e.Message, "TryToConnect");
                return;
            }
            catch (SocketException ee)
            {
                //An error occurred when attempting to access the socket.
                Debug.WriteLine(ee.Message, "TryToConnect");
                return;
            }
            catch (ObjectDisposedException eee)
            {
                //The Socket has been closed.
                Debug.WriteLine(eee.Message, "TryToConnect");
                return;
            }
        }

        public void Reuse()
        {
            if (Connected)
            {
                if (conSock.Connected)
                {
                    conSock.Shutdown(SocketShutdown.Both);
                    conSock.Disconnect(true);
                }
                Connected = false;
            }
        }

    }

    
}
