﻿namespace CONTROLLER
{
    public enum DataType
    {
        Bitmap,
        Key,
        Mouse,
        Wheel,
        Pos,
        BlockInput
    }
}
